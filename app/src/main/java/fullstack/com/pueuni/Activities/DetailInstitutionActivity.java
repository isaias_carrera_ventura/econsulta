package fullstack.com.pueuni.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import fullstack.com.pueuni.Models.Institution;
import fullstack.com.pueuni.R;

public class DetailInstitutionActivity extends AppCompatActivity {

    @BindView(R.id.imageViewInstitution)
    ImageView imageViewInstitution;
    @BindView(R.id.institutionTextView)
    TextView institutionTextView;
    @BindView(R.id.nameTextview)
    TextView nameTextview;
    @BindView(R.id.typeTextview)
    TextView typeTextview;
    @BindView(R.id.townTextview)
    TextView townTextview;
    @BindView(R.id.cityTextview)
    TextView cityTextview;

    @BindView(R.id.aliasTextView)
    TextView aliasTextView;
    @BindView(R.id.claveTextView)
    TextView claveTextView;
    @BindView(R.id.representativeTextView)
    TextView representativeTextView;
    @BindView(R.id.totalInscriptionTextView)
    TextView totalInscriptionTextView;
    @BindView(R.id.rfcTextView)
    TextView rfcTextView;
    @BindView(R.id.dateFoundTextView)
    TextView dateFoundTextView;
    @BindView(R.id.dgpTextView)
    TextView dgpTextView;
    @BindView(R.id.matriculaTextView)
    TextView matriculaTextView;
    @BindView(R.id.programActiveTextView)
    TextView programActiveTextView;

    Institution institution;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_institution);

        ButterKnife.bind(this);

        institution = (Institution) getIntent().getExtras().get("institution");

        Picasso.with(this).load(institution.getCampus()).into(imageViewInstitution);
        institutionTextView.setText(institution.getName());
        nameTextview.setText(institution.getName());
        typeTextview.setText(institution.getType());
        townTextview.setText(institution.getTown());
        cityTextview.setText(institution.getCity());

        aliasTextView.setText(institution.getAlias());
        claveTextView.setText(institution.getCenterKey());
        representativeTextView.setText(institution.getLegalRepresentative());
        totalInscriptionTextView.setText(institution.getTotalInscription());
        rfcTextView.setText(institution.getBusinessName());
        dateFoundTextView.setText(institution.getFoundationDate());
        dgpTextView.setText(institution.getDgpRegister());
        matriculaTextView.setText(institution.getEnrollment());
        programActiveTextView.setText(institution.getActivePRograms());

    }

    @OnClick(R.id.buttonLocationInstitution)
    public void location() {

        double latitude = institution.getLat();
        double longitude = institution.getLon();
        String label = institution.getName();
        String uriBegin = "geo:" + latitude + "," + longitude;
        String query = latitude + "," + longitude + "(" + label + ")";
        String encodedQuery = Uri.encode(query);
        String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
        Uri uri = Uri.parse(uriString);
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
        startActivity(intent);

    }

    @OnClick(R.id.buttonWebInstitution)
    public void webInstitution() {

        try {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(institution.getWeb()));
            startActivity(browserIntent);
        } catch (Exception e) {
            e.printStackTrace();
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Ops")
                    .setContentText("No existe la web de esta institución")
                    .show();
        }

    }

    private final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;

    @OnClick(R.id.buttonCall)
    public void call() {


        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CALL_PHONE)) {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CALL_PHONE},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CALL_PHONE},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {

            Log.w("Mensaje", "CALL");
            makeCall();

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    makeCall();

                } else {


                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }
                return;
            }

        }
    }

    private void makeCall() {

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(this.getResources().getString(R.string.aviso))
                .setContentText(this.getResources().getString(R.string.pregunta_llamada))
                .setConfirmText(this.getResources().getString(R.string.dialog_ok))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + institution.getPhone()));
                        if (ActivityCompat.checkSelfPermission(DetailInstitutionActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        startActivity(callIntent);
                    }
                })
                .setCancelText(this.getResources().getString(R.string.cancelar))
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                    }
                })
                .show();

    }

    @OnClick(R.id.buttonEmail)
    public void email() {

        /* Create the Intent */
        final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

        /* Fill it with Data */
        emailIntent.setType("plain/text");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{institution.getEmail()});

        /* Send it off to the Activity-Chooser */
        startActivity(Intent.createChooser(emailIntent, "Send mail..."));

    }

    @OnClick(R.id.buttonCheckIn)
    public void checkIn() {

        Intent intent = new Intent(this, CheckInActivity.class);
        intent.putExtra("institution", institution);
        startActivity(intent);

    }

    @OnClick(R.id.fb)
    public void fb() {
        try {

            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(institution.getFacebook()));
            startActivity(browserIntent);

        } catch (Exception e) {

            e.printStackTrace();
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Ops")
                    .setContentText("No existe el Facebook de esta institución")
                    .show();
        }
    }

    @OnClick(R.id.tw)
    public void tw() {
        try {

            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(institution.getTwitter()));
            startActivity(browserIntent);

        } catch (Exception e) {

            e.printStackTrace();
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Ops")
                    .setContentText("No existe el Twitter de esta institución")
                    .show();
        }

    }

    @OnClick(R.id.ig)
    public void ig() {
        try {

            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(institution.getInstangram()));
            startActivity(browserIntent);

        } catch (Exception e) {

            e.printStackTrace();
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Ops")
                    .setContentText("No existe el Instagram de esta institución")
                    .show();
        }
    }

    @OnClick(R.id.lk)
    public void lk() {

        try {

            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(institution.getLinkedin()));
            startActivity(browserIntent);

        } catch (Exception e) {
            e.printStackTrace();
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Ops")
                    .setContentText("No existe el Linkedin de esta institución")
                    .show();
        }
    }

}
