package fullstack.com.pueuni.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.Login;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import fullstack.com.pueuni.Controllers.InstitutionController;
import fullstack.com.pueuni.Controllers.ProgramController;
import fullstack.com.pueuni.Controllers.UserController;
import fullstack.com.pueuni.Models.UserModel;
import fullstack.com.pueuni.R;
import fullstack.com.pueuni.Utils.Constants;
import fullstack.com.pueuni.Utils.CustomProgressDialog;
import fullstack.com.pueuni.Utils.MySingleton;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.login_button)
    Button button;
    CallbackManager callbackManager;
    private final int MY_PERMISSIONS_REQUEST_GPS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setupView();
        setupForUserLocation();

    }

    private void setupView() {
        ButterKnife.bind(this);

        callbackManager = CallbackManager.Factory.create();

        // Callback registration
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                // App code
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {

                                LoginManager.getInstance().logOut();
                                makeRequestForLogin(object, response);

                            }
                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, name, link, email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
                new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.cancel_session_init))
                        .show();
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.something_went_wrong))
                        .show();
            }
        });

    }

    private void makeRequestForLogin(final JSONObject object, GraphResponse response) {

        try {

            final CustomProgressDialog customProgressDialog = new CustomProgressDialog(LoginActivity.this);
            customProgressDialog.showCustomProgressDialog();

            final String idFacebokUser = object.getString("id");

            Log.w("Mensaje", idFacebokUser);
            Log.w("Mensaje", Constants.LOGIN_ENDPOINT);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.LOGIN_ENDPOINT, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    customProgressDialog.dismissCustomProgressDialog();
                    UserModel userModel = UserController.getUserLogin(response, object);

                    if (userModel != null) {

                        if (userModel.isUserRegisterFlag()) {

                            requestForUser(userModel);

                        } else {

                            requestForUpdateIncompleteFieldsPreviousRegister(userModel);

                        }

                    } else {

                        // App code
                        new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText(getString(R.string.ops))
                                .setContentText(getString(R.string.something_went_wrong))
                                .show();

                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // App code
                    error.printStackTrace();
                    customProgressDialog.dismissCustomProgressDialog();
                    new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.something_went_wrong))
                            .show();
                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("facebook_id", idFacebokUser);
                    return params;
                }

            };

            MySingleton.getInstance(LoginActivity.this).addToRequestQueue(stringRequest);

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    private void requestForUpdateIncompleteFieldsPreviousRegister(final UserModel userModelForUpdate) {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(LoginActivity.this);
        customProgressDialog.showCustomProgressDialog();
        String url = Constants.USER_ENDPOINT + userModelForUpdate.getIdUser();

        StringRequest stringRequest = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();

                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                intent.putExtra("user", userModelForUpdate);
                startActivity(intent);
                finish();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                Log.w("Mensaje", new String(error.networkResponse.data));

                new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.something_went_wrong))
                        .show();

                customProgressDialog.dismissCustomProgressDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("name", userModelForUpdate.getName());
                params.put("email", userModelForUpdate.getEmail() != null ? userModelForUpdate.getEmail() : "");
                params.put("avatar", userModelForUpdate.getImageUrl());
                params.put("facebook_user", userModelForUpdate.getName());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "JWT " + userModelForUpdate.getJwt());
                return params;
            }
        };

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }

    private void requestForUser(final UserModel userModel) {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
        customProgressDialog.showCustomProgressDialog();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.USER_ENDPOINT + userModel.getIdUser(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();

                UserModel userModelToSave = UserController.getUserFromResponse(response, userModel.getJwt());

                if (userModelToSave != null) {

                    /*userModelToSave.save();*/
                    UserController.saveUser(LoginActivity.this, userModelToSave);

                    new DownloadUserFavorites().execute();
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    finish();

                } else {

                    new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.something_went_wrong))
                            .show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.something_went_wrong))
                        .show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "JWT " + userModel.getJwt());
                return params;
            }
        };

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }

    public void setupForUserLocation() {

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_GPS);

            } else {

                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_GPS);

            }
        }
    }

    @OnClick(R.id.login_button)
    public void registerFacebook(View view) {

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private class DownloadUserFavorites extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {

            String urlProgram = Constants.USER_ENDPOINT + UserController.getUserInDatabase().getIdUser() + "/programs";
            StringRequest stringRequestProgram = new StringRequest(Request.Method.GET, urlProgram, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    ProgramController.saveListForFavoriteProgram(ProgramController.getProgramListByType(response));
                }
            }, null) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("Authorization", "JWT " + UserController.getUserInDatabase().getJwt());
                    return params;
                }
            };

            String urlUni = Constants.USER_ENDPOINT + UserController.getUserInDatabase().getIdUser() + "/universities";
            StringRequest stringRequestUniversity = new StringRequest(Request.Method.GET, urlUni, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    InstitutionController.saveListForFavoriteUniversity(InstitutionController.getInstitutionListFromResponse(response));
                }
            }, null) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("Authorization", "JWT " + UserController.getUserInDatabase().getJwt());
                    return params;
                }
            };

            MySingleton.getInstance(LoginActivity.this).addToRequestQueue(stringRequestProgram);
            MySingleton.getInstance(LoginActivity.this).addToRequestQueue(stringRequestUniversity);

            return null;
        }
    }

}
