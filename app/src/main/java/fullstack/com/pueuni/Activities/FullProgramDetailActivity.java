package fullstack.com.pueuni.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import fullstack.com.pueuni.Controllers.InstitutionController;
import fullstack.com.pueuni.Models.Institution;
import fullstack.com.pueuni.Models.Program;
import fullstack.com.pueuni.R;
import fullstack.com.pueuni.Utils.Constants;
import fullstack.com.pueuni.Utils.CustomProgressDialog;
import fullstack.com.pueuni.Utils.MySingleton;

public class FullProgramDetailActivity extends AppCompatActivity {

    @BindView(R.id.textMode)
    TextView modeTextView;
    @BindView(R.id.textLevel)
    TextView textLevel;
    @BindView(R.id.textArea)
    TextView textArea;
    @BindView(R.id.textNameUni)
    TextView textNameUni;
    @BindView(R.id.rvoeTextView)
    TextView rvoeTextView;
    @BindView(R.id.noRvoeTextView)
    TextView noRvoeTextView;
    @BindView(R.id.turnTextView)
    TextView turnTextView;
    @BindView(R.id.statusProgramTextView)
    TextView statusProgramTextView;
    @BindView(R.id.zoneTextView)
    TextView zoneTextView;
    @BindView(R.id.updateTextView)
    TextView updateTextView;


    Program program;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_program_detail);

        ButterKnife.bind(this);

        program = (Program) getIntent().getExtras().get("program");

        modeTextView.setText(program.getMode());
        textLevel.setText(program.getLevel());
        textArea.setText(program.getArea());
        textNameUni.setText(program.getUniversityName());

        rvoeTextView.setText(program.getRvoe());
        noRvoeTextView.setText(program.getNoRvoe());
        turnTextView.setText(program.getTurn());
        statusProgramTextView.setText(program.getStatus());
        zoneTextView.setText(program.getZone());
        updateTextView.setText(program.getLastUpdate());

    }

    @OnClick(R.id.buttonMore)
    public void goToInstitution() {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.UNIVERSITIES_ENDPOINT + "/" + program.getUniversityId(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();

                Institution institution = InstitutionController.getInstitutionFromResponse(response);
                if (institution != null) {

                    Intent intent = new Intent(FullProgramDetailActivity.this, DetailInstitutionActivity.class);
                    intent.putExtra("institution", institution);
                    startActivity(intent);

                } else {

                    new SweetAlertDialog(FullProgramDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.aviso))
                            .setContentText(getString(R.string.something_went_wrong))
                            .show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                new SweetAlertDialog(FullProgramDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.aviso))
                        .setContentText(getString(R.string.something_went_wrong))
                        .show();

                customProgressDialog.dismissCustomProgressDialog();
            }
        });

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
        customProgressDialog.showCustomProgressDialog();

    }
}
