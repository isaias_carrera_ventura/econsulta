package fullstack.com.pueuni.Activities;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.clans.fab.FloatingActionMenu;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import fullstack.com.pueuni.Adapters.ProgramAdapter;
import fullstack.com.pueuni.Controllers.ProgramController;
import fullstack.com.pueuni.Models.Program;
import fullstack.com.pueuni.R;
import fullstack.com.pueuni.Utils.Constants;
import fullstack.com.pueuni.Utils.MySingleton;

public class ProgramDetailActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    private List<Program> programList = new ArrayList<>();
    @BindView(R.id.recycler_view_program_public)
    RecyclerView recyclerView;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.menu)
    FloatingActionMenu menu;

    private int page = 0;

    private static final String TAG_REQUEST = "ProgramFragment";
    private ProgramAdapter mAdapter;

    String query = "";
    private boolean isLoading = false;
    private boolean isLastPage = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program_detail);

        ButterKnife.bind(this);

        query = getIntent().getExtras().getString("query") == null ? "" : "?" + getIntent().getExtras().getString("query");

        swipeRefreshLayout.setOnRefreshListener(this);
        mAdapter = new ProgramAdapter(programList, this);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

                super.onScrollStateChanged(recyclerView, newState);

                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();

                int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();

                if (!isLoading && !isLastPage) {

                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0) {

                        page = page + 1;
                        requestForProgram();

                    }

                }
            }
        });

        requestForProgram();


    }


    private void requestForProgram() {

        if (!swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(true);
        }

        isLoading = true;

        String pageStr = query.equals("") ? "?page=" : "&page=";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.PROGRAMS_ENDPOINT + query + pageStr + page, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                swipeRefreshLayout.setRefreshing(false);
                isLoading = false;

                List<Program> programListAux = ProgramController.getProgramListByType(response);
                if (programListAux != null) {

                    if (programListAux.size() > 0) {

                        programList.addAll(programListAux);
                        mAdapter.notifyDataSetChanged();

                    } else {

                        isLastPage = true;

                        if (page == 0) {

                            new SweetAlertDialog(ProgramDetailActivity.this, SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText(getString(R.string.ops))
                                    .setContentText(getString(R.string.no_results_near))
                                    .show();
                        }


                    }

                } else {

                    new SweetAlertDialog(ProgramDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.something_went_wrong))
                            .show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                swipeRefreshLayout.setRefreshing(false);
                isLoading = false;
                isLastPage = false;

                new SweetAlertDialog(ProgramDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.something_went_wrong))
                        .show();
            }
        });


        stringRequest.setRetryPolicy(Constants.getPolicty());
        stringRequest.setTag(TAG_REQUEST);
        MySingleton.getInstance(ProgramDetailActivity.this).addToRequestQueue(stringRequest);
    }

    @Override
    public void onRefresh() {

        requestForProgram();

    }

    @OnClick(R.id.allPrograms)
    public void allProgram() {

        menu.close(true);

        isLastPage = false;
        page = 0;
        programList.clear();
        requestForProgram();

    }


    @OnClick(R.id.favoritesPrograms)
    public void favoritesPrograms() {

        List<Program> programListAux = ProgramController.getFavoritePrograms();
        menu.close(true);

        if (programListAux != null) {

            if (programListAux.size() > 0) {

                programList.clear();
                programList.addAll(programListAux);
                mAdapter.notifyDataSetChanged();

            } else {

                new SweetAlertDialog(ProgramDetailActivity.this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.no_favorite_programs))
                        .show();

            }

        } else {

            new SweetAlertDialog(ProgramDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText(getString(R.string.ops))
                    .setContentText(getString(R.string.something_went_wrong))
                    .show();

        }

    }

}
