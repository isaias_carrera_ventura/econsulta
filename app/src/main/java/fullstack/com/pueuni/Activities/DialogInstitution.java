package fullstack.com.pueuni.Activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import fullstack.com.pueuni.Controllers.InstitutionController;
import fullstack.com.pueuni.Models.Institution;
import fullstack.com.pueuni.R;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class DialogInstitution extends Activity {


    @BindView(R.id.rankingSchool)
    MaterialRatingBar materialRatingBar;
    @BindView(R.id.emailInstitition)
    TextView textViewEmail;
    @BindView(R.id.webInstitition)
    TextView textViewWebInstitition;
    @BindView(R.id.townInstitition)
    TextView textViewTownInstitition;
    @BindView(R.id.nameInstitition)
    TextView textViewName;
    @BindView(R.id.imageViewInstitution)
    CircleImageView imageView;
    Institution institution;
    @BindView(R.id.institutionView)
    LinearLayout institutionView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_institution);
        ButterKnife.bind(this);

        institution = (Institution) getIntent().getExtras().getSerializable("institution");
        textViewEmail.setText(institution.getEmail());
        textViewWebInstitition.setText(institution.getWeb());
        textViewTownInstitition.setText(institution.getTown());
        textViewName.setText(institution.getName());
        materialRatingBar.setMax(5);
        materialRatingBar.setEnabled(false);
        materialRatingBar.setProgress(institution.getRank());
        Picasso.with(this).load(institution.getLogo()).into(imageView);


    }


    @OnClick(R.id.institutionView)
    public void detailView() {
        Intent intent = new Intent(this, DetailInstitutionActivity.class);
        intent.putExtra("institution", institution);
        startActivity(intent);
    }

    @OnClick(R.id.fb)
    public void fb() {
        try {

            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(institution.getFacebook()));
            startActivity(browserIntent);

        } catch (Exception e) {

            e.printStackTrace();
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Ops")
                    .setContentText("No existe el Facebook de esta institución")
                    .show();
        }
    }

    @OnClick(R.id.tw)
    public void tw() {
        try {

            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(institution.getTwitter()));
            startActivity(browserIntent);

        } catch (Exception e) {

            e.printStackTrace();
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Ops")
                    .setContentText("No existe el Twitter de esta institución")
                    .show();
        }

    }

    @OnClick(R.id.ig)
    public void ig() {
        try {

            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(institution.getInstangram()));
            startActivity(browserIntent);

        } catch (Exception e) {

            e.printStackTrace();
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Ops")
                    .setContentText("No existe el Instagram de esta institución")
                    .show();
        }
    }

    @OnClick(R.id.lk)
    public void lk() {

        try {

            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(institution.getLinkedin()));
            startActivity(browserIntent);

        } catch (Exception e) {
            e.printStackTrace();
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Ops")
                    .setContentText("No existe el Linkedin de esta institución")
                    .show();
        }
    }
}
