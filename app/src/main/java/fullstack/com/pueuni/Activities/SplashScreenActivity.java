package fullstack.com.pueuni.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import fullstack.com.pueuni.Controllers.UserController;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (UserController.getUserInDatabase() != null) {

            startActivity(new Intent(this, MainActivity.class));

        } else {

            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);

        }

        finish();

    }

}
