package fullstack.com.pueuni.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import fullstack.com.pueuni.Models.Program;
import fullstack.com.pueuni.R;
import fullstack.com.pueuni.Utils.Constants;

public class TestResultActivity extends AppCompatActivity {

    @BindView(R.id.textViewFirstResult)
    TextView textViewFirstResult;
    @BindView(R.id.textViewSecondResult)
    TextView textViewSecondResult;
    @BindView(R.id.textViewThirdResult)
    TextView textViewThirdResult;

    List<String> stringResult;

    boolean flagActivated = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_result);

        ButterKnife.bind(this);

        flagActivated = getIntent().getBooleanExtra(Constants.FIRST_TIME_OPEN, false);
        stringResult = getIntent().getExtras().getStringArrayList("result");

        try {

            textViewFirstResult.setText(stringResult.get(0));
            textViewSecondResult.setText(stringResult.get(1));
            textViewThirdResult.setText(stringResult.get(2));

        } catch (Exception e) {
            TestResultActivity.this.finish();
        }


    }

    @OnClick({R.id.buttonOption1, R.id.buttonOption2, R.id.buttonOption3})
    public void option(View view) {

        Intent intent = new Intent(this, ProgramDetailActivity.class);

        switch (view.getId()) {
            case R.id.buttonOption1:
                intent.putExtra("query", "area=" + stringResult.get(0));
                break;

            case R.id.buttonOption2:
                intent.putExtra("query", "area=" + stringResult.get(1));
                break;

            case R.id.buttonOption3:
                intent.putExtra("query", "area=" + stringResult.get(2));
                break;

        }

        startActivity(intent);

    }

    @Override
    public void onBackPressed() {

        if (flagActivated) {

            Intent setIntent = new Intent(TestResultActivity.this, MainActivity.class);
            startActivity(setIntent);
            finish();

        } else {

            finish();

        }


    }
}
