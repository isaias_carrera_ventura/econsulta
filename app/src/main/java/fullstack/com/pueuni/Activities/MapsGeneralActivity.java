package fullstack.com.pueuni.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import fullstack.com.pueuni.Controllers.InstitutionController;
import fullstack.com.pueuni.Controllers.UserController;
import fullstack.com.pueuni.Models.Institution;
import fullstack.com.pueuni.R;
import fullstack.com.pueuni.Utils.Constants;
import fullstack.com.pueuni.Utils.CustomProgressDialog;
import fullstack.com.pueuni.Utils.MySingleton;

public class MapsGeneralActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbarCheckin;
    @BindView(R.id.menu)
    FloatingActionMenu menu;
    @BindView(R.id.myTownUniversities)
    FloatingActionButton myTownUniversities;


    private static final String TAG = "RecyclerViewFragment";
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    protected final static String KEY_LOCATION = "location";
    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;
    protected LocationSettingsRequest mLocationSettingsRequest;
    protected Location mCurrentLocation;

    private final int MY_PERMISSIONS_REQUEST_GPS = 1;
    SweetAlertDialog sweetAlertDialog;
    SweetAlertDialog sweetAlertDialogActiveLocation;

    private GoogleMap googleMap;
    private List<Institution> institutionList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_general);

        setupView();

        try {
            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);

        } catch (Exception e) {
            e.printStackTrace();
        }

        sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("¡Activa tu ubicación!")
                .setContentText("Para brindarte un mejor servicio")
                .setConfirmText("Ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                        sDialog.dismissWithAnimation();
                        sweetAlertDialog.dismiss();
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                });

        sweetAlertDialogActiveLocation = new
                SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("¡Activa tu ubicación!")
                .setContentText("Para brindarte un mejor servicio")
                .setConfirmText("Ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sweetAlertDialogActiveLocation.dismissWithAnimation();
                        sDialog.dismissWithAnimation();
                        sweetAlertDialogActiveLocation.dismiss();
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getApplicationContext().getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);
                    }
                });


        updateValuesFromBundle(savedInstanceState);
        // Kick off the process of building the GoogleApiClient, LocationRequest, and
        // LocationSettingsRequest objects.
        buildGoogleApiClient();
        createLocationRequest();
        buildLocationSettingsRequest();

    }

    private void setupView() {

        ButterKnife.bind(this);

        setSupportActionBar(toolbarCheckin);
        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

        }

        try {
            myTownUniversities.setLabelText(UserController.getUserInDatabase().getTown());
        } catch (Exception e) {
            myTownUniversities.setLabelText(getString(R.string.estado));
        }

    }


    public void setupForUserLocation() {

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_GPS);

            } else {

               /* if (!sweetAlertDialogActiveLocation.isShowing()) {
                    sweetAlertDialogActiveLocation.show();
                }*/
                new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Oops!")
                        .setContentText(getString(R.string.location_active))
                        .show();

            }
        } else {

            startLocationUpdates();

        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_GPS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    startLocationUpdates();

                } else {
                    sweetAlertDialog.show();
                }
            }
        }
    }

    /**
     * Updates fields based on data stored in the bundle.
     *
     * @param savedInstanceState The activity state saved in the Bundle.
     */
    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {

            // Update the value of mCurrentLocation from the Bundle and update the UI to show the
            // correct latitude and longitude.
            if (savedInstanceState.keySet().contains(KEY_LOCATION)) {

                // Since KEY_LOCATION was found in the Bundle, we can be sure that mCurrentLocation
                // is not null.
                mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION);

            }
        }
    }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
     * LocationServices API.
     */

    protected synchronized void buildGoogleApiClient() {

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

    }

    protected void createLocationRequest() {

        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    }

    protected void buildLocationSettingsRequest() {

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();

    }

    /**
     * Requests location updates from the FusedLocationApi.
     */
    protected void startLocationUpdates() {

        LocationServices.SettingsApi.checkLocationSettings(
                mGoogleApiClient,
                mLocationSettingsRequest
        ).setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, MapsGeneralActivity.this);
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                        if (!sweetAlertDialog.isShowing()) {
                            sweetAlertDialog.show();
                        }

                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        String errorMessage = "Location settings are inadequate, and cannot be " +
                                "fixed here. Fix in Settings.";

                        Toast.makeText(MapsGeneralActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    /**
     * Updates all UI fields.
     */
    private void makeRequestToServer() {

        if (mCurrentLocation != null & googleMap != null) {

            try {

                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), 12.0f));

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {

            setupForUserLocation();

        }

    }


    @Override
    public void onMapReady(GoogleMap map) {

        googleMap = map;

        // For showing a move to my location button
        if (ActivityCompat.checkSelfPermission(MapsGeneralActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MapsGeneralActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setScrollGesturesEnabled(true);
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.setOnMarkerClickListener(this);

    }


    /**
     * Removes location updates from the FusedLocationApi.
     */
    protected void stopLocationUpdates() {
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient,
                this
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                //setButtonsEnabledState();
            }
        });
    }


    @Override
    public void onStop() {

        super.onStop();
        mGoogleApiClient.disconnect();

    }

    @Override
    public void onStart() {

        super.onStart();
        mGoogleApiClient.connect();

    }

    @Override
    protected void onResume() {
        super.onResume();
        setupForUserLocation();
    }

    @Override
    public void onPause() {
        super.onPause();
        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
    }

    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    @Override
    public void onConnected(Bundle connectionHint) {

        if (mCurrentLocation == null) {

            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            if (googleMap != null && mCurrentLocation != null) {

                makeRequestToServer();
                onLocationChanged(mCurrentLocation);
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), 12));

                String query = "lat=" + (mCurrentLocation.getLatitude()) + "&lon=" + (mCurrentLocation.getLongitude());
                requestForUniversities(query, 12);

            }

        } else {

            requestForUniversities("all=true", 12);

        }

    }

    /**
     * Callback that fires when the location changes.
     */
    @Override
    public void onLocationChanged(Location location) {

        mCurrentLocation = location;

    }

    @Override
    public void onConnectionSuspended(int cause) {

        Log.w("Mensaje", "onConnectionSuspended" + cause);

    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.w("Mensaje", "onConnectionFailed" + result.toString());
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putParcelable(KEY_LOCATION, mCurrentLocation);
        super.onSaveInstanceState(savedInstanceState);
    }

    @OnClick(R.id.favorites)
    public void favorites(View view) {

        if (menu.isOpened()) menu.close(true);

        List<Institution> institutionList = Institution.listAll(Institution.class);
        if (institutionList.size() > 0) {

            drawMarkersInMap(Institution.listAll(Institution.class), 10);

        } else {

            new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(getString(R.string.ops))
                    .setContentText(getString(R.string.no_favorite_college))
                    .show();
        }

    }

    @OnClick(R.id.allUniversitiesMap)
    public void showAll(View view) {

        String query = "all=true";
        requestForUniversities(query, 8);

    }

    @OnClick(R.id.nearMyLocation)
    public void nearMyLocation(View view) {

        if (mCurrentLocation != null) {

            String query = "lat=" + (mCurrentLocation.getLatitude()) + "&lon=" + (mCurrentLocation.getLongitude());
            requestForUniversities(query, 8);

        } else {

            // App code
            new SweetAlertDialog(MapsGeneralActivity.this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(getString(R.string.ops))
                    .setContentText(getString(R.string.location_active))
                    .show();


        }

    }

    @OnClick(R.id.myTownUniversities)
    public void myTownUniversities(View view) {

        String query = "town=" + UserController.getUserInDatabase().getTown();
        requestForUniversities(query, 10);

    }

    private void requestForUniversities(String query, final int sizeForZoom) {

        if (menu.isOpened()) menu.close(true);


        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
        customProgressDialog.showCustomProgressDialog();
        String url = Constants.UNIVERSITIES_ENDPOINT_QUERY + "?" + query;

        Log.w("Mensaje", url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();
                List<Institution> institutionList = InstitutionController.getInstitutionListFromResponse(response);

                if (institutionList != null) {
                    if (institutionList.size() > 0) {

                        drawMarkersInMap(institutionList, sizeForZoom);

                    } else {

                        new SweetAlertDialog(MapsGeneralActivity.this, SweetAlertDialog.WARNING_TYPE)
                                .setTitleText(getString(R.string.ops))
                                .setContentText(getString(R.string.no_results_near))
                                .show();
                    }
                } else {
                    new SweetAlertDialog(MapsGeneralActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.something_went_wrong))
                            .show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                // App code
                error.printStackTrace();

                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(MapsGeneralActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.something_went_wrong))
                        .show();
            }
        });


        stringRequest.setRetryPolicy(Constants.getPolicty());
        MySingleton.getInstance(this).addToRequestQueue(stringRequest);


    }

    private void drawMarkersInMap(List<Institution> institutionList, int sizeForZoom) {

        if (mCurrentLocation != null)
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), sizeForZoom));

        googleMap.clear();
        this.institutionList = institutionList;
        for (int i = 0; i < institutionList.size(); i++) {
            Marker marker = googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(institutionList.get(i).getLat(), institutionList.get(i).getLon()))
                    .title(institutionList.get(i).getName())
            );
            marker.setTag(i);
        }
    }


    @Override
    public boolean onMarkerClick(Marker marker) {

        if (marker.getTag() != null) {

            Institution institution = institutionList.get((Integer) marker.getTag());
            Intent i = new Intent(this, DialogInstitution.class);
            i.putExtra("institution", institution);
            startActivity(i);

        }
        return false;

    }
}
