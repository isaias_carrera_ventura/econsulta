package fullstack.com.pueuni.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.OnClick;
import fullstack.com.pueuni.R;

public class MenuTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_test);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.psicometric)
    public void psicometric(View vieW) {
        Intent i = new Intent(this, TestActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.socioeconmic)
    public void socioeconmic(View vieW) {
        Intent i = new Intent(this, TestActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.vocacional)
    public void vocacional(View vieW) {
        Intent i = new Intent(this, TestActivity.class);
        startActivity(i);
    }


}
