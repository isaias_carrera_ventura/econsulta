package fullstack.com.pueuni.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.OnClick;
import fullstack.com.pueuni.Controllers.UserController;
import fullstack.com.pueuni.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.buttonProfile)
    public void profile(View view) {
        startActivity(new Intent(this, ProfileActivity.class));
    }

    @OnClick(R.id.buttonMenuTest)
    public void menuTest(View view) {
        startActivity(new Intent(this, TestActivity.class));
    }

    @OnClick(R.id.buttonMapGeneral)
    public void mapGeneral(View view) {
        startActivity(new Intent(this, MapsGeneralActivity.class));
    }

    @OnClick(R.id.buttonInstitutions)
    public void institutions(View view) {
        startActivity(new Intent(this, ProgramInstitutionsActivity.class));
    }

    @OnClick(R.id.buttonCheckIn)
    public void checkIn(View view) {
        startActivity(new Intent(this, CheckInActivity.class));
    }

    @OnClick(R.id.buttonPrograms)
    public void programs(View view) {
        startActivity(new Intent(this, ProgramDetailActivity.class));
    }

}
