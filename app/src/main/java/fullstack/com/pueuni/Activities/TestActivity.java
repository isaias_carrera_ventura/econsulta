package fullstack.com.pueuni.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import fullstack.com.pueuni.Adapters.TestQuestionAdapter;
import fullstack.com.pueuni.Controllers.TestController;
import fullstack.com.pueuni.Controllers.UserController;
import fullstack.com.pueuni.Models.VocationalQuestion;
import fullstack.com.pueuni.R;
import fullstack.com.pueuni.Utils.Constants;
import fullstack.com.pueuni.Utils.CustomProgressDialog;
import fullstack.com.pueuni.Utils.MySingleton;

public class TestActivity extends AppCompatActivity {

    private List<VocationalQuestion> vocationalQuestions = new ArrayList<>();

    @BindView(R.id.recycler_test_question_voc)
    RecyclerView recyclerView;
    private TestQuestionAdapter mAdapter;

    boolean flagActivated = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        ButterKnife.bind(this);

        mAdapter = new TestQuestionAdapter(vocationalQuestions);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        flagActivated = getIntent().getBooleanExtra(Constants.FIRST_TIME_OPEN, false);

        prepareQuesitonData();
    }

    private void prepareQuesitonData() {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.TEST_ENDPOINT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();

                List<VocationalQuestion> vocationalQuestionsServer = TestController.getTestQuestions(response);
                if (vocationalQuestionsServer != null) {

                    mAdapter = new TestQuestionAdapter(vocationalQuestionsServer);
                    recyclerView.setAdapter(mAdapter);

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                new SweetAlertDialog(TestActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.aviso))
                        .setContentText(getString(R.string.something_went_wrong))
                        .setConfirmText(getString(R.string.salir))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {

                                sweetAlertDialog.dismissWithAnimation();
                                TestActivity.this.finish();


                            }
                        })
                        .show();

                customProgressDialog.dismissCustomProgressDialog();


                error.printStackTrace();

            }
        });

        stringRequest.setRetryPolicy(Constants.getPolicty());
        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
        customProgressDialog.showCustomProgressDialog();


    }

    @Override
    public void onBackPressed() {

        new SweetAlertDialog(TestActivity.this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(getString(R.string.aviso))
                .setContentText(getString(R.string.salir_test))
                .setConfirmText(getString(R.string.salir))
                .setCancelText(getString(R.string.continuar))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {

                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                        sweetAlertDialog.dismissWithAnimation();
                        if (flagActivated) {

                            Intent setIntent = new Intent(TestActivity.this, MainActivity.class);
                            startActivity(setIntent);
                            finish();

                        } else {

                            finish();

                        }

                    }
                })
                .show();


    }

    @OnClick(R.id.computeResButton)
    public void computeResult(View view) {

        int[] answers = mAdapter.getListAnswer();
        List<VocationalQuestion> listResult = mAdapter.getListResult();
        boolean isFinished = true;

        for (int i = 0; i < answers.length; i++) {

            if (answers[i] == -1) {
                isFinished = false;
            }

        }

        if (!isFinished) {

            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText(getString(R.string.ops))
                    .setContentText(getString(R.string.answer_all_warning))
                    .show();

        } else {

            sendResultToServer(answers, listResult);

        }

    }


    public void sendResultToServer(int[] answers, List<VocationalQuestion> listResult) {
        try {

            JSONObject json = new JSONObject();
            JSONArray jsonArray = new JSONArray();

            for (int i = 0; i < answers.length; i++) {

                JSONObject jsonObject = new JSONObject();

                String id = listResult.get(i).getId();
                int value = listResult.get(i).getAnswers()[answers[i]].getValue();

                jsonObject.put("question", id);
                jsonObject.put("value", value);

                jsonArray.put(jsonObject);

            }

            json.put("response", jsonArray);

            final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);

            final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Constants.TEST_ENDPOINT + UserController.getUserInDatabase().getIdUser(), json, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    customProgressDialog.dismissCustomProgressDialog();

                    try {

                        JSONArray jsonArrayResul = response.getJSONArray("result");
                        ArrayList<String> resultString = new ArrayList<>();

                        for (int i = 0; i < jsonArrayResul.length(); i++) {

                            resultString.add((String) jsonArrayResul.get(i));

                        }

                        Intent intent = new Intent(TestActivity.this, TestResultActivity.class);
                        intent.putExtra("result", resultString);
                        intent.putExtra(Constants.FIRST_TIME_OPEN, flagActivated);
                        startActivity(intent);
                        TestActivity.this.finish();

                    } catch (Exception e) {
                        new SweetAlertDialog(TestActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText(getString(R.string.ops))
                                .setContentText(getString(R.string.answer_all_warning))
                                .show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    new SweetAlertDialog(TestActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.answer_all_warning))
                            .show();

                    customProgressDialog.dismissCustomProgressDialog();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    HashMap<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    return params;

                }
            };

            MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
            customProgressDialog.showCustomProgressDialog();

        } catch (Exception e) {

            e.printStackTrace();

        }
    }
}
