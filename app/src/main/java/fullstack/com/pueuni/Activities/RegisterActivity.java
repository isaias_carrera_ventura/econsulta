package fullstack.com.pueuni.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Pattern;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import fullstack.com.pueuni.Controllers.InterestAreaController;
import fullstack.com.pueuni.Controllers.TownController;
import fullstack.com.pueuni.Controllers.UserController;
import fullstack.com.pueuni.Models.UserModel;
import fullstack.com.pueuni.R;
import fullstack.com.pueuni.Utils.Constants;
import fullstack.com.pueuni.Utils.CustomProgressDialog;
import fullstack.com.pueuni.Utils.MySingleton;

public class RegisterActivity extends AppCompatActivity implements Validator.ValidationListener {

    @BindView(R.id.townSpinner)
    Spinner spinnerTownSpinner;

    @BindView(R.id.interestAreaSpinner)
    Spinner spinnerAreaSpinner;

/*    @BindView(R.id.interestCareerSpinner)
    Spinner spinnerInterestCareer;*/

    @NotEmpty(trim = true, messageResId = R.string.field_required)
    @BindView(R.id.nameUser)
    EditText nameUserEditText;

    @NotEmpty(trim = true, messageResId = R.string.field_required)
    @BindView(R.id.interestCareer)
    EditText interestCareer;

    @NotEmpty(trim = true, messageResId = R.string.origin_school_required)
    @BindView(R.id.originSchool)
    EditText editTextOriginSchool;

    @NotEmpty(trim = true, messageResId = R.string.field_required)
    @Email(messageResId = R.string.email_required)
    @BindView(R.id.emailUser)
    EditText editTextEmail;

    @NotEmpty(trim = true, messageResId = R.string.field_required)
    @Pattern(regex = "[0-9]+", messageResId = R.string.match_required_regex)
    @BindView(R.id.ageUser)
    EditText ageUserEditText;

    ArrayAdapter<String> adapterArea;
    List<String> interestAreas;

    ArrayAdapter<String> adapterTown;
    List<String> towns;

    UserModel userModel;
    private Validator validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        ButterKnife.bind(this);
        userModel = (UserModel) getIntent().getExtras().getSerializable("user");
        setupView(userModel);

        getTownsFromServer();
        getInterestAreas();

    }

    private void setupView(UserModel userModel) {

        validator = new Validator(this);
        validator.setValidationListener(this);

        nameUserEditText.setText(userModel.getName());

        if (userModel.getEmail() != null)
            editTextEmail.setText(userModel.getEmail());

        //TOWNS
        towns = new ArrayList<>();
        towns.add(getResources().getString(R.string.estado));
        adapterTown = new ArrayAdapter<>(RegisterActivity.this, android.R.layout.simple_spinner_item, towns);
        adapterTown.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTownSpinner.setAdapter(adapterTown);

        spinnerTownSpinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN && towns.size() == 1) {
                    getTownsFromServer();
                }

                return false;
            }
        });

        //INTEREST AREAS
        interestAreas = new ArrayList<>();
        interestAreas.add(getResources().getString(R.string.area_estudio));
        adapterArea = new ArrayAdapter<>(RegisterActivity.this, android.R.layout.simple_spinner_item, interestAreas);
        adapterArea.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAreaSpinner.setAdapter(adapterArea);

        spinnerAreaSpinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN && interestAreas.size() == 1) {
                    getInterestAreas();
                }

                return false;
            }
        });

    }

    private void getInterestAreas() {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(RegisterActivity.this);
        customProgressDialog.showCustomProgressDialog();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.AREAS_ENDPOINT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();

                List<String> stringListAreas = InterestAreaController.getInterestAreas(response);
                if (stringListAreas != null) {

                    interestAreas.clear();
                    stringListAreas.add(0, getResources().getString(R.string.area_estudio));
                    interestAreas.addAll(stringListAreas);
                    adapterArea.notifyDataSetChanged();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                customProgressDialog.dismissCustomProgressDialog();

            }
        });

        MySingleton.getInstance(RegisterActivity.this).addToRequestQueue(stringRequest);

    }

    private void getTownsFromServer() {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(RegisterActivity.this);
        customProgressDialog.showCustomProgressDialog();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.TOWNS_ENDPOINT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();
                List<String> townList = TownController.getTownList(response);
                if (townList != null) {

                    towns.clear();
                    towns.add(0, getResources().getString(R.string.estado));
                    towns.addAll(townList);
                    adapterTown.notifyDataSetChanged();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                customProgressDialog.dismissCustomProgressDialog();

            }
        });

        MySingleton.getInstance(RegisterActivity.this).addToRequestQueue(stringRequest);
    }


    int optionSelected = 0;


    @OnClick(R.id.noTestMethod)
    public void skipTest(View view) {

        optionSelected = 1;
        validator.validate();

    }

    @OnClick(R.id.doTestMethod)
    public void doTestMethod(View view) {

        optionSelected = 2;
        validator.validate();

    }

    @Override
    public void onValidationSucceeded() {

        if (spinnerAreaSpinner.getSelectedItemPosition() == 0 || spinnerTownSpinner.getSelectedItemPosition() == 0) {

            new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText(getString(R.string.ops))
                    .setContentText(getString(R.string.select_options_spinner))
                    .show();

        } else {

            if (optionSelected == 1) {

                requestForUpdateUserInformation(new Intent(RegisterActivity.this, MainActivity.class));

            } else {

                Intent intent = new Intent(this, TestActivity.class);
                intent.putExtra(Constants.FIRST_TIME_OPEN, true);
                requestForUpdateUserInformation(intent);

            }

        }

    }

    private void requestForUpdateUserInformation(final Intent intent) {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(RegisterActivity.this);
        customProgressDialog.showCustomProgressDialog();
        String url = Constants.USER_ENDPOINT + userModel.getIdUser();

        StringRequest stringRequest = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();

                userModel.setAge(Integer.parseInt(ageUserEditText.getText().toString()));
                userModel.setTown(towns.get(spinnerTownSpinner.getSelectedItemPosition()));
                userModel.setSchool(editTextOriginSchool.getText().toString());
                userModel.setArea(interestAreas.get(spinnerAreaSpinner.getSelectedItemPosition()));
                userModel.setInterest_career(interestCareer.getText().toString());
                userModel.setEmail(editTextEmail.getText().toString());

                /*userModel.save();*/

                UserController.saveUser(RegisterActivity.this, userModel);

                startActivity(intent);
                RegisterActivity.this.finish();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                customProgressDialog.dismissCustomProgressDialog();

                new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.something_went_wrong))
                        .show();


            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("name", userModel.getName());
                params.put("age", ageUserEditText.getText().toString());
                params.put("town", towns.get(spinnerTownSpinner.getSelectedItemPosition()));
                params.put("school", editTextOriginSchool.getText().toString());
                params.put("area", interestAreas.get(spinnerAreaSpinner.getSelectedItemPosition()));
                params.put("interest_carrer", interestCareer.getText().toString());
                params.put("email", editTextEmail.getText().toString());
                params.put("avatar", userModel.getImageUrl());
                params.put("facebook_user", userModel.getName());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "JWT " + userModel.getJwt());
                return params;
            }
        };

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }


}
