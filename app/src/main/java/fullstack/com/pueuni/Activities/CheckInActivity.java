package fullstack.com.pueuni.Activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import fullstack.com.pueuni.Controllers.InstitutionController;
import fullstack.com.pueuni.Models.Institution;
import fullstack.com.pueuni.R;
import fullstack.com.pueuni.Utils.Constants;
import fullstack.com.pueuni.Utils.CustomProgressDialog;
import fullstack.com.pueuni.Utils.ImageUtils;
import fullstack.com.pueuni.Utils.MySingleton;
import fullstack.com.pueuni.Utils.VolleyMultipartRequest;
import id.zelory.compressor.Compressor;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class CheckInActivity extends AppCompatActivity implements Validator.ValidationListener {

    @BindView(R.id.rankingSchool)
    MaterialRatingBar rankingSchool;
    @BindView(R.id.institutionViewNotSelected)
    RelativeLayout relativeLayoutNotSelected;
    @BindView(R.id.labelName)
    TextView labelName;
    @BindView(R.id.labelCity)
    TextView labelCity;
    @BindView(R.id.labelWeb)
    TextView labelWeb;
    @BindView(R.id.labelMail)
    TextView labelMail;
    @BindView(R.id.imageViewInstitution)
    CircleImageView imageView;

    @BindView(R.id.loadViewForCheckin)
    LinearLayout loadViewForCheckin;

    @NotEmpty(trim = true, messageResId = R.string.field_required)
    @BindView(R.id.editTextComment)
    EditText editTextComment;
    @BindView(R.id.spinnerUniversities)
    Spinner spinnerList;
    @BindView(R.id.question_1)
    MaterialRatingBar questionOne;
    @BindView(R.id.question_2)
    MaterialRatingBar questionTwo;
    @BindView(R.id.question_3)
    MaterialRatingBar questionThree;
    @BindView(R.id.question_4)
    MaterialRatingBar questionFour;
    Boolean flagRatingBarChanged[] = {false, false, false, false};

    List<Institution> institutionList = new ArrayList<>();
    Institution currentInstitution;
    List<String> stringList = new ArrayList<>();

    private Validator validator;
    private boolean imageLoaded;
    private String path;
    private ArrayAdapter adapterUniversities;
    private int QRCODE_ACTIVITY = 1;
    byte[] bytePhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in);
        setupView();

        fillSpinnerWithUniversities();

        if (getIntent().getExtras().get("institution") != null) {
            fillViewWithUniversity((Institution) getIntent().getExtras().get("institution"));
        }


    }

    private void setupView() {

        ButterKnife.bind(this);

        rankingSchool.setEnabled(false);
        validator = new Validator(this);
        validator.setValidationListener(this);

        questionOne.setOnRatingChangeListener(new MaterialRatingBar.OnRatingChangeListener() {
            @Override
            public void onRatingChanged(MaterialRatingBar ratingBar, float rating) {
                flagRatingBarChanged[0] = true;
            }
        });
        questionTwo.setOnRatingChangeListener(new MaterialRatingBar.OnRatingChangeListener() {
            @Override
            public void onRatingChanged(MaterialRatingBar ratingBar, float rating) {
                flagRatingBarChanged[1] = true;
            }
        });
        questionThree.setOnRatingChangeListener(new MaterialRatingBar.OnRatingChangeListener() {
            @Override
            public void onRatingChanged(MaterialRatingBar ratingBar, float rating) {
                flagRatingBarChanged[2] = true;
            }
        });
        questionFour.setOnRatingChangeListener(new MaterialRatingBar.OnRatingChangeListener() {
            @Override
            public void onRatingChanged(MaterialRatingBar ratingBar, float rating) {
                flagRatingBarChanged[3] = true;
            }
        });


        spinnerList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                if (position != 0) {

                    fillViewWithUniversity(institutionList.get(position - 1));

                } else {

                    //removeViewForSelection();

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //TOWNS
        stringList = new ArrayList<>();
        institutionList = new ArrayList<>();
        stringList.add(getResources().getString(R.string.seleccionar_instituci_n));
        adapterUniversities = new ArrayAdapter<>(CheckInActivity.this, android.R.layout.simple_spinner_item, stringList);
        adapterUniversities.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerList.setAdapter(adapterUniversities);


        spinnerList.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN && stringList.size() == 1) {

                    fillSpinnerWithUniversities();

                }

                return false;
            }
        });


    }

    private void removeViewForSelection() {

        relativeLayoutNotSelected.setVisibility(View.VISIBLE);
        loadViewForCheckin.setVisibility(View.INVISIBLE);

    }

    private void fillViewWithUniversity(Institution institution) {

        currentInstitution = institution;

        loadViewForCheckin.setVisibility(View.VISIBLE);
        relativeLayoutNotSelected.setVisibility(View.INVISIBLE);

        rankingSchool.setProgress(institution.getRank());
        labelCity.setText(institution.getCity() + " " + institution.getTown());
        labelWeb.setText(institution.getWeb());
        labelName.setText(institution.getName());
        labelMail.setText(institution.getEmail());
        Picasso.with(this).load(institution.getLogo()).into(imageView);

    }


    private void fillSpinnerWithUniversities() {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
        customProgressDialog.showCustomProgressDialog();
        String url = Constants.UNIVERSITIES_ENDPOINT_QUERY + "?all=true";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();

                List<Institution> institutionListFromserver = InstitutionController.getInstitutionListFromResponse(response);

                if (institutionListFromserver.size() > 0) {

                    institutionList.clear();
                    institutionList.addAll(institutionListFromserver);
                    stringList.clear();

                    stringList.add(getResources().getString(R.string.seleccionar_instituci_n));

                    for (int i = 0; i < institutionList.size(); i++) {

                        stringList.add(institutionList.get(i).getName());

                    }

                    adapterUniversities.notifyDataSetChanged();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                // App code
                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(CheckInActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.something_went_wrong))
                        .show();

            }
        });


        MySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }

    @Override
    public void onValidationSucceeded() {

        if (!isRatingBarChanged()) {

            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText(getString(R.string.ops))
                    .setContentText(getString(R.string.rating_bar_missed))
                    .show();

        } else if (imageLoaded) {

            requestForUpload();
            requestForCheckin();

        } else {

            requestForCheckin();

        }

    }

    private void requestForUpload() {

        final String url = Constants.CHECKIN_ENDPOINT_PHOTO + currentInstitution.getIdInstitution();

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.PUT, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {

                Log.w("Mensaje", response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

            }
        }) {
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, VolleyMultipartRequest.DataPart> params = new HashMap<>();
                params.put("photo", new DataPart("img.jpg", bytePhoto, "image/jpeg"));
                return params;
            }
        };

        MySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);

    }

    private void requestForCheckin() {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.CHECKIN_ENDPOINT + currentInstitution.getIdInstitution(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                new SweetAlertDialog(CheckInActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText(getString(R.string.ok))
                        .setContentText(getString(R.string.ranking_success))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                                CheckInActivity.this.finish();
                            }
                        })
                        .show();

                customProgressDialog.dismissCustomProgressDialog();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                new SweetAlertDialog(CheckInActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.something_went_wrong))
                        .show();

                customProgressDialog.dismissCustomProgressDialog();
                Log.w("Mensaje", error.toString());

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<>();
                map.put("atention", String.valueOf(questionOne.getRating()));
                map.put("clarity", String.valueOf(questionTwo.getRating()));
                map.put("suficiency", String.valueOf(questionThree.getRating()));
                map.put("evaluation", String.valueOf(questionFour.getRating()));
                map.put("comments", editTextComment.getText().toString());
                return map;

            }
        };

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
        customProgressDialog.showCustomProgressDialog();

    }

    public boolean isRatingBarChanged() {
        boolean isComplete = true;
        for (Boolean aFlagRatingBarChanged : flagRatingBarChanged) {
            if (!aFlagRatingBarChanged) {
                isComplete = false;
            }
        }
        return isComplete;
    }

    @OnClick(R.id.button_send)
    public void sendCheckIn(View view) {
        validator.validate();
    }

    @OnClick(R.id.buttonImageCheckIn)
    public void imageCheckIn(View view) {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setMaxCropResultSize(1500, 1500)
                .start(this);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {

                Uri resultUri = result.getUri();
                path = ImageUtils.getRealPathFromURI_API19(this, resultUri);
                File file = new File(path);
                Bitmap compressedImageBitmap = Compressor.getDefault(this).compressToBitmap(file);

                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                compressedImageBitmap.compress(Bitmap.CompressFormat.PNG, 0, byteArrayOutputStream);
                bytePhoto = byteArrayOutputStream.toByteArray();
                imageLoaded = true;


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                error.printStackTrace();
            }
        } else if (requestCode == QRCODE_ACTIVITY) {

            if (resultCode == RESULT_OK) {

                currentInstitution = (Institution) data.getExtras().get("institution");
                fillViewWithUniversity(currentInstitution);
            }

        }

    }

    @OnClick(R.id.qrScanner)
    public void qr() {

        Intent i = new Intent(this, QRCodeActivity.class);
        startActivityForResult(i, QRCODE_ACTIVITY);

    }


}
