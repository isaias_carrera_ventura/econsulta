package fullstack.com.pueuni.Activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import fullstack.com.pueuni.Controllers.InstitutionController;
import fullstack.com.pueuni.Models.Institution;
import fullstack.com.pueuni.R;
import fullstack.com.pueuni.Utils.Constants;
import fullstack.com.pueuni.Utils.CustomProgressDialog;
import fullstack.com.pueuni.Utils.MySingleton;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QRCodeActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView mScannerView;
    private final int MY_PERMISSIONS_REQUEST_CAMERA = 5;
    private ViewGroup placeholder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_qrcode);
        View v = findViewById(R.id.qrScanner);
        placeholder = (ViewGroup) v;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mScannerView != null) {
            mScannerView.stopCamera();
            mScannerView.stopCameraPreview();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

            if (mScannerView == null) {
                mScannerView = new ZXingScannerView(this);
                mScannerView.setResultHandler(this);
            }

            placeholder.removeAllViews();
            if (mScannerView.getParent() != null) {
                ((ViewGroup) mScannerView.getParent()).removeView(mScannerView);
            }
            placeholder.addView(mScannerView);
            mScannerView.startCamera();

        } else {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CAMERA)) {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_CAMERA);


            } else {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_CAMERA);

            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (mScannerView == null) {
                        mScannerView = new ZXingScannerView(this);
                        mScannerView.setResultHandler(this);
                    }

                    placeholder.removeAllViews();
                    if (mScannerView.getParent() != null) {
                        ((ViewGroup) mScannerView.getParent()).removeView(mScannerView);
                    }
                    placeholder.addView(mScannerView);
                    mScannerView.startCamera();

                }
            }
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mScannerView != null) {
            mScannerView.stopCamera();
        }
    }


    @OnClick(R.id.buttonConcede)
    public void goToSettings(View view) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.fromParts("package", this.getPackageName(), null));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
    }

    @Override
    public void handleResult(final com.google.zxing.Result rawResult) {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.UNIVERSITIES_ENDPOINT + "/" + rawResult.toString(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();

                Institution institution = InstitutionController.getInstitutionFromResponse(response);

                if (institution != null) {

                    Log.w("Mensaje", institution.getName());

                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("institution", institution);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();

                } else {

                    new SweetAlertDialog(QRCodeActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.something_went_wrong))
                            .show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (mScannerView != null) {
                    // If you would like to resume scanning, call this method below:
                    mScannerView.resumeCameraPreview(QRCodeActivity.this);
                }

                new SweetAlertDialog(QRCodeActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.something_went_wrong))
                        .show();

                customProgressDialog.dismissCustomProgressDialog();

            }
        });

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
        customProgressDialog.showCustomProgressDialog();

        Log.w("Mensaje", rawResult.toString());


    }
}
