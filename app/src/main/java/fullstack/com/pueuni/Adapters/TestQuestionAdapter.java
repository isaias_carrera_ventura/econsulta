package fullstack.com.pueuni.Adapters;

import android.support.annotation.IdRes;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fullstack.com.pueuni.Models.VocationalQuestion;
import fullstack.com.pueuni.R;

/**
 * Created by chaycv on 21/06/17.
 */

public class TestQuestionAdapter extends RecyclerView.Adapter<TestQuestionAdapter.MyViewHolder> {

    private List<VocationalQuestion> vocationalQuestions;
    private int[] answers;


    public TestQuestionAdapter(List<VocationalQuestion> vocationalQuestions) {

        this.vocationalQuestions = vocationalQuestions;
        this.answers = new int[vocationalQuestions.size()];
        for (int i = 0; i < answers.length; i++) {
            this.answers[i] = 0;
        }

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_test_question, parent, false);
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        VocationalQuestion vocationalQuestion = vocationalQuestions.get(position);

        holder.textViewQuestion.setText(vocationalQuestion.getQuestion());
        holder.first.setText(vocationalQuestion.getAnswers()[0].getTitle());
        holder.second.setText(vocationalQuestion.getAnswers()[1].getTitle());
        holder.third.setText(vocationalQuestion.getAnswers()[2].getTitle());

        holder.radioGroup.setTag(position);


        try {
            switch (answers[position]) {
                case 0:
                    holder.radioGroup.check(R.id.first);
                    break;
                case 1:
                    holder.radioGroup.check(R.id.second);
                    break;
                case 2:
                    holder.radioGroup.check(R.id.third);
                    break;
                default:
                    holder.radioGroup.clearCheck();
                    break;
            }
        } catch (Exception ignored) {

        }
    }

    @Override
    public int getItemCount() {
        return vocationalQuestions.size();
    }

    public List<VocationalQuestion> getListResult() {
        return vocationalQuestions;
    }

    public int[] getListAnswer() {
        return answers;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.first)
        RadioButton first;
        @BindView(R.id.second)
        RadioButton second;
        @BindView(R.id.third)
        RadioButton third;
        @BindView(R.id.textViewQuestion)
        TextView textViewQuestion;
        @BindView(R.id.radioGroupAnswer)
        RadioGroup radioGroup;


        public MyViewHolder(View view) {

            super(view);
            ButterKnife.bind(this, view);

            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {

                    int tag = (int) group.getTag();

                    if (checkedId == R.id.first) {

                        answers[tag] = 0;

                    } else if (checkedId == R.id.second) {

                        answers[tag] = 1;

                    } else if (checkedId == R.id.third) {

                        answers[tag] = 2;

                    } else {

                        answers[tag] = -1;

                    }

                }
            });

        }
    }

}
