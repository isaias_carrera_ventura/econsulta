package fullstack.com.pueuni.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import fullstack.com.pueuni.Activities.DetailInstitutionActivity;
import fullstack.com.pueuni.Controllers.InstitutionController;
import fullstack.com.pueuni.Controllers.UserController;
import fullstack.com.pueuni.Models.Institution;
import fullstack.com.pueuni.Models.VocationalQuestion;
import fullstack.com.pueuni.R;
import fullstack.com.pueuni.Utils.Constants;
import fullstack.com.pueuni.Utils.MySingleton;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

/**
 * Created by chaycv on 28/06/17.
 */

public class InstitutionAdapter extends RecyclerView.Adapter<InstitutionAdapter.MyViewHolder> {

    private List<Institution> institutionList;
    private Context context;

    public InstitutionAdapter(List<Institution> institutionListAux, Context context) {

        this.context = context;
        this.institutionList = institutionListAux;

    }

    @Override
    public InstitutionAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_institution, parent, false);
        return new InstitutionAdapter.MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(InstitutionAdapter.MyViewHolder holder, final int position) {

        final Institution institution = institutionList.get(position);
        holder.textViewEmail.setText(institution.getEmail());
        holder.textViewWebInstitition.setText(institution.getWeb());
        holder.textViewTownInstitition.setText(institution.getTown());
        holder.textViewName.setText(institution.getName());
        holder.materialRatingBar.setProgress(institution.getRank());
        Picasso.with(context).load(institution.getLogo()).into(holder.imageView);
        holder.checkBox.setChecked(InstitutionController.isInstitutionFavorite(institution));

    }

    @Override
    public int getItemCount() {
        return institutionList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.rankingSchool)
        MaterialRatingBar materialRatingBar;
        @BindView(R.id.emailInstitition)
        TextView textViewEmail;
        @BindView(R.id.webInstitition)
        TextView textViewWebInstitition;
        @BindView(R.id.townInstitition)
        TextView textViewTownInstitition;
        @BindView(R.id.nameInstitition)
        TextView textViewName;
        @BindView(R.id.imageViewInstitution)
        CircleImageView imageView;
        @BindView(R.id.favorite)
        CheckBox checkBox;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            materialRatingBar.setMax(5);
            materialRatingBar.setEnabled(false);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, DetailInstitutionActivity.class);
                    intent.putExtra("institution", institutionList.get(getAdapterPosition()));
                    context.startActivity(intent);

                }
            });

            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, final boolean b) {
                    final String url;

                    if (b && !InstitutionController.isInstitutionFavorite(institutionList.get(getAdapterPosition()))) {

                        InstitutionController.saveFavorite(institutionList.get(getAdapterPosition()));
                        url = Constants.USER_ENDPOINT + UserController.getUserInDatabase().getIdUser() + "/fav";
                        makeRequest(url, institutionList.get(getAdapterPosition()), b);

                    } else if (!b && InstitutionController.isInstitutionFavorite(institutionList.get(getAdapterPosition()))) {

                        InstitutionController.removeFromFavorites(institutionList.get(getAdapterPosition()));
                        url = Constants.USER_ENDPOINT + UserController.getUserInDatabase().getIdUser() + "/unfav";
                        makeRequest(url, institutionList.get(getAdapterPosition()), b);

                    }

                }
            });

        }

        public void makeRequest(String url, final Institution institution, final boolean b) {


            StringRequest stringRequest = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    if (b) {
                        InstitutionController.saveFavorite(institution);
                    } else {
                        InstitutionController.removeFromFavorites(institution);
                    }

                    new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText(context.getString(R.string.change_made))
                            .show();


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(context.getString(R.string.ops))
                            .setContentText(context.getString(R.string.something_went_wrong))
                            .show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("university", institution.getIdInstitution());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("Authorization", "JWT " + UserController.getUserInDatabase().getJwt());
                    return params;
                }
            };

            MySingleton.getInstance(context).addToRequestQueue(stringRequest);

        }

        @OnClick(R.id.fb)
        public void fb() {
            try {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(institutionList.get(getAdapterPosition()).getFacebook()));
                context.startActivity(browserIntent);

            } catch (Exception e) {

                e.printStackTrace();
                new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Ops")
                        .setContentText("No existe el Facebook de esta institución")
                        .show();
            }
        }

        @OnClick(R.id.tw)
        public void tw() {
            try {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(institutionList.get(getAdapterPosition()).getTwitter()));
                context.startActivity(browserIntent);

            } catch (Exception e) {

                e.printStackTrace();
                new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Ops")
                        .setContentText("No existe el Twitter de esta institución")
                        .show();
            }

        }

        @OnClick(R.id.ig)
        public void ig() {
            try {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(institutionList.get(getAdapterPosition()).getInstangram()));
                context.startActivity(browserIntent);

            } catch (Exception e) {

                e.printStackTrace();
                new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Ops")
                        .setContentText("No existe el Instagram de esta institución")
                        .show();
            }
        }

        @OnClick(R.id.lk)
        public void lk() {

            try {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(institutionList.get(getAdapterPosition()).getLinkedin()));
                context.startActivity(browserIntent);

            } catch (Exception e) {
                e.printStackTrace();
                new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Ops")
                        .setContentText("No existe el Linkedin de esta institución")
                        .show();
            }
        }

    }
}

