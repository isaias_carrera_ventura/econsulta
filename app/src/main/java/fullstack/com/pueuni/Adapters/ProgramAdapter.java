package fullstack.com.pueuni.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import fullstack.com.pueuni.Activities.FullProgramDetailActivity;
import fullstack.com.pueuni.Controllers.InstitutionController;
import fullstack.com.pueuni.Controllers.ProgramController;
import fullstack.com.pueuni.Controllers.UserController;
import fullstack.com.pueuni.Models.Institution;
import fullstack.com.pueuni.Models.Program;
import fullstack.com.pueuni.R;
import fullstack.com.pueuni.Utils.Constants;
import fullstack.com.pueuni.Utils.MySingleton;

/**
 * Created by chaycv on 05/07/17.
 */

public class ProgramAdapter extends RecyclerView.Adapter<ProgramAdapter.MyViewHolder> {

    private List<Program> programList;
    private Context context;

    public ProgramAdapter(List<Program> programList, Context context) {

        this.context = context;
        this.programList = programList;

    }

    @Override
    public ProgramAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_program, parent, false);
        return new ProgramAdapter.MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(ProgramAdapter.MyViewHolder holder, int position) {

        Program program = programList.get(position);
        holder.textViewName.setText(program.getName());
        holder.textViewAreaProgram.setText(program.getArea());
        holder.textViewUniProgram.setText(program.getUniversityName());
        holder.checkBox.setChecked(ProgramController.isProgramFavorite(program));
        Picasso.with(context).load(program.getUniversityLogo()).into(holder.imageViewLogo);

    }

    @Override
    public int getItemCount() {
        return programList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.nameProgram)
        TextView textViewName;
        @BindView(R.id.areaProgram)
        TextView textViewAreaProgram;
        @BindView(R.id.uniProgram)
        TextView textViewUniProgram;
        @BindView(R.id.favoriteProgram)
        CheckBox checkBox;
        @BindView(R.id.imageViewLogo)
        CircleImageView imageViewLogo;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, FullProgramDetailActivity.class);
                    intent.putExtra("program", programList.get(getAdapterPosition()));
                    context.startActivity(intent);

                }
            });


            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                    String url;

                    if (b && !ProgramController.isProgramFavorite(programList.get(getAdapterPosition()))) {

                        ProgramController.saveFavorite(programList.get(getAdapterPosition()));
                        url = Constants.USER_ENDPOINT + UserController.getUserInDatabase().getIdUser() + "/favprogram";
                        makeRequest(url, programList.get(getAdapterPosition()), b);


                    } else if (!b && ProgramController.isProgramFavorite(programList.get(getAdapterPosition()))) {

                        ProgramController.removeFromFavorites(programList.get(getAdapterPosition()));
                        url = Constants.USER_ENDPOINT + UserController.getUserInDatabase().getIdUser() + "/unfavprogram";
                        makeRequest(url, programList.get(getAdapterPosition()), b);

                    }


                }
            });
        }

        public void makeRequest(String url, final Program program, final boolean b) {


            StringRequest stringRequest = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    if (b) {
                        ProgramController.saveFavorite(program);
                    } else if (ProgramController.isProgramFavorite(program)) {
                        ProgramController.removeFromFavorites(program);
                    }

                           /* new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText(context.getString(R.string.change_made))
                                    .show();*/


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(context.getString(R.string.ops))
                            .setContentText(context.getString(R.string.something_went_wrong))
                            .show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("program", program.getIdProgram());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("Authorization", "JWT " + UserController.getUserInDatabase().getJwt());
                    return params;
                }
            };

            MySingleton.getInstance(context).addToRequestQueue(stringRequest);

        }
    }
}
