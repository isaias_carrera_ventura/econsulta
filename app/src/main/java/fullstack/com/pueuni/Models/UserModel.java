package fullstack.com.pueuni.Models;

import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * Created by chaycv on 26/07/17.
 */

public class UserModel extends SugarRecord implements Serializable {

    String idUser;
    String idFacebook;
    String email;
    String name;
    String imageUrl;
    String jwt;
    boolean userRegisterFlag;

    int age;
    String town;
    String school;
    String area;
    String interest_career;

    public UserModel() {
    }

    public UserModel(String idUser, String idFacebook, String email, String name, String imageUrl, String jwt, boolean userRegisterFlag, int age, String town, String school, String area,
                     String interest_career) {
        this.idUser = idUser;
        this.idFacebook = idFacebook;
        this.email = email;
        this.name = name;
        this.imageUrl = imageUrl;
        this.jwt = jwt;
        this.userRegisterFlag = userRegisterFlag;
        //
        this.age = age;
        this.town = town;
        this.school = school;
        this.area = area;
        this.interest_career = interest_career;

    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getIdUser() {
        return idUser;
    }

    public String getIdFacebook() {
        return idFacebook;
    }

    public String getJwt() {
        return jwt;
    }

    public boolean isUserRegisterFlag() {
        return userRegisterFlag;
    }


    public int getAge() {
        return age;
    }

    public String getTown() {
        return town;
    }

    public String getSchool() {
        return school;
    }

    public String getArea() {
        return area;
    }

    public String getInterest_career() {
        return interest_career;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public void setIdFacebook(String idFacebook) {
        this.idFacebook = idFacebook;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public void setUserRegisterFlag(boolean userRegisterFlag) {
        this.userRegisterFlag = userRegisterFlag;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setInterest_career(String interest_career) {
        this.interest_career = interest_career;
    }
}
