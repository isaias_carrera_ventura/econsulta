package fullstack.com.pueuni.Models;

import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * Created by chaycv on 05/07/17.
 */

public class Program extends SugarRecord implements Serializable {


    String idProgram;
    String name;
    String area;
    String mode;
    String level;
    String universityId;
    String universityName;
    String universityLogo;

    String rvoe;
    String noRvoe;
    String turn;
    String zone;
    String status;
    String lastUpdate;

    public Program() {
    }

    public Program(String idProgram, String name, String area, String mode, String level, String universityId, String universityName, String universityLogo, String rvoe, String noRvoe, String turn, String zone, String status, String lastUpdate) {
        this.idProgram = idProgram;
        this.name = name;
        this.area = area;
        this.mode = mode;
        this.level = level;
        this.universityId = universityId;
        this.universityName = universityName;
        this.universityLogo = universityLogo;
        this.rvoe = rvoe;
        this.noRvoe = noRvoe;
        this.turn = turn;
        this.zone = zone;
        this.status = status;
        this.lastUpdate = lastUpdate;
    }

    public String getUniversityLogo() {
        return universityLogo;
    }

    public String getIdProgram() {
        return idProgram;
    }

    public String getName() {
        return name;
    }

    public String getArea() {
        return area;
    }

    public String getMode() {
        return mode;
    }

    public String getLevel() {
        return level;
    }

    public String getUniversityId() {
        return universityId;
    }

    public String getUniversityName() {
        return universityName;
    }

    public String getRvoe() {
        return rvoe;
    }

    public String getNoRvoe() {
        return noRvoe;
    }

    public String getTurn() {
        return turn;
    }

    public String getZone() {
        return zone;
    }

    public String getStatus() {
        return status;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }
}
