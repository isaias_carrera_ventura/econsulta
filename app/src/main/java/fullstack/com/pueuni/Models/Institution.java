package fullstack.com.pueuni.Models;

import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * Created by chaycv on 28/06/17.
 */

public class Institution extends SugarRecord implements Serializable {

    String idInstitution;
    String name, type, logo;
    int rank;
    String town, city, web, email, phone, facebook, twitter, instangram, linkedin;
    Double lat, lon;
    String campus;
    String alias, centerKey, legalRepresentative, totalInscription, businessName, foundationDate, dgpRegister, enrollment, activePRograms;

    public Institution() {
    }

    public Institution(String idInstitution, String name, String type, String logo,
                       int rank, String town, String city, String web, String email,
                       String phone, String facebook, String twitter, String instangram,
                       String linkedin, Double lat, Double lon, String alias, String centerKey,
                       String legalRepresentative, String totalInscription, String businessName,
                       String foundationDate, String dgpRegister, String enrollment, String activePRograms, String campusImage) {
        this.idInstitution = idInstitution;
        this.name = name;
        this.type = type;
        this.logo = logo;
        this.rank = rank;
        this.town = town;
        this.city = city;
        this.web = web;
        this.email = email;
        this.phone = phone;
        this.facebook = facebook;
        this.twitter = twitter;
        this.instangram = instangram;
        this.linkedin = linkedin;
        this.lat = lat;
        this.lon = lon;
        this.alias = alias;
        this.centerKey = centerKey;
        this.legalRepresentative = legalRepresentative;
        this.totalInscription = totalInscription;
        this.businessName = businessName;
        this.foundationDate = foundationDate;
        this.dgpRegister = dgpRegister;
        this.enrollment = enrollment;
        this.activePRograms = activePRograms;
        this.campus = campusImage;
    }


    public String getCampus() {
        return campus;
    }

    public String getIdInstitution() {
        return idInstitution;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getLogo() {
        return logo;
    }

    public int getRank() {
        return rank;
    }

    public String getTown() {
        return town;
    }

    public String getCity() {
        return city;
    }

    public String getWeb() {
        return web;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getFacebook() {
        return facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public String getInstangram() {
        return instangram;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLon() {
        return lon;
    }


    public String getAlias() {
        return alias;
    }

    public String getCenterKey() {
        return centerKey;
    }

    public String getLegalRepresentative() {
        return legalRepresentative;
    }

    public String getTotalInscription() {
        return totalInscription;
    }

    public String getBusinessName() {
        return businessName;
    }

    public String getFoundationDate() {
        return foundationDate;
    }

    public String getDgpRegister() {
        return dgpRegister;
    }

    public String getEnrollment() {
        return enrollment;
    }

    public String getActivePRograms() {
        return activePRograms;
    }
}
