package fullstack.com.pueuni.Models;

/**
 * Created by chaycv on 21/06/17.
 */

public class VocationalQuestion {

    private String question;
    private String id;
    private Answer[] answers;

    public VocationalQuestion(String question, String id, Answer[] answers) {
        this.question = question;
        this.id = id;
        this.answers = answers;
    }

    public String getQuestion() {
        return question;
    }

    public String getId() {
        return id;
    }

    public Answer[] getAnswers() {
        return answers;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setAnswers(Answer[] answers) {
        this.answers = answers;
    }

    public static class Answer {

        private String title;
        private int value;

        public Answer(String title, int value) {
            this.title = title;
            this.value = value;
        }

        public String getTitle() {
            return title;
        }

        public int getValue() {
            return value;
        }
    }

}
