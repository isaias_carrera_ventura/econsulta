package fullstack.com.pueuni.Controllers;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import fullstack.com.pueuni.Models.VocationalQuestion;

/**
 * Created by chaycv on 11/12/17.
 */

public class TestController {


    public static List<VocationalQuestion> getTestQuestions(String response) {

        try {

            JSONArray jsonArray = new JSONArray(response);
            List<VocationalQuestion> vocationalQuestions = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String id = jsonObject.getString("id");
                String question = jsonObject.getString("question");

                VocationalQuestion.Answer[] array = new VocationalQuestion.Answer[3];

                JSONArray jsonArrayValue = jsonObject.getJSONArray("answers");
                for (int j = 0; j < jsonArrayValue.length(); j++) {

                    JSONObject jsonObjectQ = jsonArrayValue.getJSONObject(j);
                    String title = jsonObjectQ.getString("title");
                    int value = jsonObjectQ.getInt("value");
                    array[j] = new VocationalQuestion.Answer(title, value);

                }

                vocationalQuestions.add(new VocationalQuestion(question, id, array));


            }

            return vocationalQuestions;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
