package fullstack.com.pueuni.Controllers;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import fullstack.com.pueuni.Models.Institution;
import fullstack.com.pueuni.Models.Program;

/**
 * Created by chaycv on 05/09/17.
 */

public class ProgramController {

    public static List<Program> getProgramListByType(String response) {

        try {

            List<Program> programs = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("programs");

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject jsonObjectUni = jsonArray.getJSONObject(i);
                String id = jsonObjectUni.getString("id");
                String name = jsonObjectUni.getString("name");
                String area = jsonObjectUni.getString("area");
                String mode = jsonObjectUni.getString("mode");
                String level = jsonObjectUni.getString("level");
                String university = jsonObjectUni.getString("university");
                String university_name = jsonObjectUni.getString("university_name");
                String university_logo = jsonObjectUni.getString("university_logo");

                String rvoe = jsonObjectUni.getString("rvoe");
                String noRvoe = jsonObjectUni.getString("no_rvoe");
                String turn = jsonObjectUni.getString("turn");
                String zone = jsonObjectUni.getString("zone");
                String status = jsonObjectUni.getString("status");
                String lastUpdate = jsonObjectUni.getString("last_update");

                Program program = new Program(id, name, area, mode, level, university, university_name, university_logo, rvoe, noRvoe, turn, zone, status, lastUpdate);
                programs.add(program);

            }

            return programs;
        } catch (Exception e) {
            return null;
        }

    }

    public static void removeFromFavorites(Program program) {

        List<Program> programList = Program.listAll(Program.class);

        for (int i = 0; i < programList.size(); i++) {

            if (programList.get(i).getIdProgram().equals(program.getIdProgram())) {

                programList.get(i).delete();
                break;

            }

        }

    }

    public static void saveFavorite(Program program) {

        Log.w("Mensaje", "SaveFavorite");
        program.save();

    }

    public static boolean isProgramFavorite(Program program) {
        List<Program> programList = Program.listAll(Program.class);
        for (int i = 0; i < programList.size(); i++) {
            if (program.getIdProgram().equals(programList.get(i).getIdProgram()))
                return true;
        }

        return false;
    }

    public static void saveListForFavoriteProgram(List<Program> programList) {

        for (Program program : programList) {
            saveFavorite(program);
        }

    }

    public static List<Program> getFavoritePrograms() {

        return Program.listAll(Program.class);

    }
}
