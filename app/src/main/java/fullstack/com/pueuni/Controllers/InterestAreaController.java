package fullstack.com.pueuni.Controllers;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chaycv on 26/07/17.
 */

public class InterestAreaController {

    public static List<String> getInterestAreas(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);
            JSONArray interestAreas = jsonObject.getJSONArray("areas");
            List<String> stringArrayList = new ArrayList<>();
            for (int i = 0; i < interestAreas.length(); i++) {
                String area = interestAreas.getString(i);
                stringArrayList.add(area);
            }

            return stringArrayList;

        } catch (Exception e) {
            return null;
        }

    }

}
