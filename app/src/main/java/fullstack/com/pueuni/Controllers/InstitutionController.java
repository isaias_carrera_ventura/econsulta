package fullstack.com.pueuni.Controllers;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import fullstack.com.pueuni.Models.Institution;
import fullstack.com.pueuni.Models.Program;

/**
 * Created by chaycv on 07/08/17.
 */

public class InstitutionController {

    public static List<Institution> getInstitutionListByType(String key, String response) {

        try {

            List<Institution> institutionList = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("universities");

            for (int i = 0; i < jsonArray.length(); i++) {


                JSONObject jsonObjectUni = jsonArray.getJSONObject(i);

                String type = jsonObjectUni.getString("type");

                if (type.equals(key)) {

                    String id = jsonObjectUni.getString("id");
                    String name = jsonObjectUni.getString("name");
                    String logo = jsonObjectUni.getString("logo");
                    int rank = jsonObjectUni.getInt("rank");
                    String town = jsonObjectUni.getString("town");
                    String city = jsonObjectUni.getString("city");
                    String web = jsonObjectUni.getString("web");
                    String email = jsonObjectUni.getString("email");
                    String phone = jsonObjectUni.getString("phone");
                    String facebook = jsonObjectUni.getString("facebook");
                    String twitter = jsonObjectUni.getString("twitter");
                    String instagram = jsonObjectUni.getString("instagram");
                    String linkedin = jsonObjectUni.getString("name");
                    double lat = jsonObjectUni.getDouble("lat");
                    double lon = jsonObjectUni.getDouble("lon");

                    String alias = jsonObjectUni.getString("alias");
                    String centerKey = jsonObjectUni.getString("center_key");
                    String legalRepre = jsonObjectUni.getString("legal_representative");
                    String totalInscription = jsonObjectUni.getString("total_inscription");
                    String businessName = jsonObjectUni.getString("business_name");
                    String foundationDate = jsonObjectUni.getString("foundation_date");
                    String dgpRegister = jsonObjectUni.getString("dgp_register");
                    String enrollment = jsonObjectUni.getString("enrollment");
                    String activeProgram = jsonObjectUni.getString("active_programs");

                    String image = jsonObjectUni.getString("image");

                    Institution institution = new Institution(id, name, type, logo, rank, town, city, web, email, phone, facebook, twitter,
                            instagram, linkedin, lat, lon, alias, centerKey, legalRepre, totalInscription, businessName, foundationDate,
                            dgpRegister, enrollment, activeProgram, image);
                    institutionList.add(institution);

                }

            }

            return institutionList;

        } catch (Exception e) {

            e.printStackTrace();
            return null;

        }

    }

    public static List<Institution> getInstitutionListFromResponse(String response) {

        try {

            List<Institution> institutionList = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("universities");

            for (int i = 0; i < jsonArray.length(); i++) {


                JSONObject jsonObjectUni = jsonArray.getJSONObject(i);

                String id = jsonObjectUni.getString("id");
                String type = jsonObjectUni.getString("type");
                String name = jsonObjectUni.getString("name");
                String logo = jsonObjectUni.getString("logo");
                int rank = jsonObjectUni.getInt("rank");
                String town = jsonObjectUni.getString("town");
                String city = jsonObjectUni.getString("city");
                String web = jsonObjectUni.getString("web");
                String email = jsonObjectUni.getString("email");
                String phone = jsonObjectUni.getString("phone");
                String facebook = jsonObjectUni.getString("facebook");
                String twitter = jsonObjectUni.getString("twitter");
                String instagram = jsonObjectUni.getString("instagram");
                String linkedin = jsonObjectUni.getString("name");
                double lat = jsonObjectUni.getDouble("lat");
                double lon = jsonObjectUni.getDouble("lon");

                String alias = jsonObjectUni.getString("alias");
                String centerKey = jsonObjectUni.getString("center_key");
                String legalRepre = jsonObjectUni.getString("legal_representative");
                String totalInscription = jsonObjectUni.getString("total_inscription");
                String businessName = jsonObjectUni.getString("business_name");
                String foundationDate = jsonObjectUni.getString("foundation_date");
                String dgpRegister = jsonObjectUni.getString("dgp_register");
                String enrollment = jsonObjectUni.getString("enrollment");
                String activeProgram = jsonObjectUni.getString("active_programs");

                String image = jsonObjectUni.getString("image");

                Institution institution = new Institution(id, name, type, logo, rank, town, city, web, email, phone, facebook, twitter,
                        instagram, linkedin, lat, lon, alias, centerKey, legalRepre, totalInscription, businessName,
                        foundationDate, dgpRegister, enrollment, activeProgram, image);
                institutionList.add(institution);

            }

            return institutionList;

        } catch (Exception e) {
            return null;
        }

    }

    public static void removeFromFavorites(Institution institution) {

        List<Institution> institutionList = Institution.listAll(Institution.class);

        for (int i = 0; i < institutionList.size(); i++) {

            if (institutionList.get(i).getIdInstitution().equals(institution.getIdInstitution())) {

                Log.w("Mensaje", institutionList.get(i).getName());

                institutionList.get(i).delete();
                break;

            }

        }

    }

    public static void saveFavorite(Institution institution) {

        institution.save();

    }

    public static boolean isInstitutionFavorite(Institution institution) {

        List<Institution> institutionList = Institution.listAll(Institution.class);
        for (int i = 0; i < institutionList.size(); i++) {
            if (institution.getIdInstitution().equals(institutionList.get(i).getIdInstitution()))
                return true;
        }

        return false;
    }

    public static void saveListForFavoriteUniversity(List<Institution> institutionListFromResponse) {
        for (Institution institution : institutionListFromResponse) {
            saveFavorite(institution);
        }
    }

    public static Institution getInstitutionFromResponse(String response) {

        try {
            JSONObject jsonObjectUni = new JSONObject(response);

            String id = jsonObjectUni.getString("id");
            String type = jsonObjectUni.getString("type");
            String name = jsonObjectUni.getString("name");
            String logo = jsonObjectUni.getString("logo");
            int rank = jsonObjectUni.getInt("rank");
            String town = jsonObjectUni.getString("town");
            String city = jsonObjectUni.getString("city");
            String web = jsonObjectUni.getString("web");
            String email = jsonObjectUni.getString("email");
            String phone = jsonObjectUni.getString("phone");
            String facebook = jsonObjectUni.getString("facebook");
            String twitter = jsonObjectUni.getString("twitter");
            String instagram = jsonObjectUni.getString("instagram");
            String linkedin = jsonObjectUni.getString("name");
            double lat = jsonObjectUni.getDouble("lat");
            double lon = jsonObjectUni.getDouble("lon");

            String alias = jsonObjectUni.getString("alias");
            String centerKey = jsonObjectUni.getString("center_key");
            String legalRepre = jsonObjectUni.getString("legal_representative");
            String totalInscription = jsonObjectUni.getString("total_inscription");
            String businessName = jsonObjectUni.getString("business_name");
            String foundationDate = jsonObjectUni.getString("foundation_date");
            String dgpRegister = jsonObjectUni.getString("dgp_register");
            String enrollment = jsonObjectUni.getString("enrollment");
            String activeProgram = jsonObjectUni.getString("active_programs");
            String image = jsonObjectUni.getString("image");

            return new Institution(id, name, type, logo, rank, town, city, web, email, phone, facebook, twitter,
                    instagram, linkedin, lat, lon, alias, centerKey, legalRepre, totalInscription, businessName, foundationDate,
                    dgpRegister, enrollment, activeProgram, image);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }


    }

    public static List<Institution> getFavoriteInstitutions() {

        return Institution.listAll(Institution.class);

    }

    public static List<Institution> getFavoriteInstitutions(String key) {

        Log.w("Mensaje", "TYPE: " + key);
        return Institution.find(Institution.class, "type = ?", key);

    }
}
