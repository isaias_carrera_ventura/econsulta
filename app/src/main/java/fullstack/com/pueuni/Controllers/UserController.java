package fullstack.com.pueuni.Controllers;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.orm.SugarApp;

import org.json.JSONObject;

import java.util.List;

import fullstack.com.pueuni.Models.Institution;
import fullstack.com.pueuni.Models.Program;
import fullstack.com.pueuni.Models.UserModel;

/**
 * Created by chaycv on 26/07/17.
 */

public class UserController {


    private static String MyPREFERENCES = "PREFS_USER";

    public static UserModel getUserLogin(String response, JSONObject jsonFacebookParams) {

        try {

            Log.w("Mensaje", jsonFacebookParams.toString());
            Log.w("Mensaje", response);

            String name = jsonFacebookParams.getString("name");
            String idFacebook = jsonFacebookParams.getString("id");
            String urlImage = "https://graph.facebook.com/" + idFacebook + "/picture?type=large";

            String email = null;
            if (jsonFacebookParams.has("email")) {
                email = jsonFacebookParams.getString("email");
            }

            //PARAMS AWS
            JSONObject jsonObject = new JSONObject(response);
            String userId = jsonObject.getString("id");
            String jwt = jsonObject.getString("jwt");
            boolean isSignedUpd = jsonObject.getInt("new") == 0;

            // int age, String town, String school, String area,String interest_career) {
            return new UserModel(userId, idFacebook, email, name, urlImage, jwt, isSignedUpd, 0, null, null, null, null);


        } catch (Exception e) {
            return null;
        }


    }

    public static UserModel getUserFromResponse(String response, String jwt) {

        try {

            Log.w("Mensaje", response);

            JSONObject jsonObject = new JSONObject(response);
            String id = jsonObject.getString("id");
            String name = jsonObject.getString("name");
            int age = jsonObject.getInt("age");
            String town = jsonObject.getString("town");
            String school = jsonObject.getString("school");
            String area = jsonObject.getString("area");
            String interestCareer = jsonObject.getString("interest_carrer");
            String facebook_id = jsonObject.getString("facebook_id");
            String email = jsonObject.getString("email");
            String avatar = jsonObject.getString("avatar");
            return new UserModel(id, facebook_id, email, name, avatar, jwt, true, age, town, school, area, interestCareer);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static void logout() {

        UserModel.deleteAll(UserModel.class);
        Institution.deleteAll(Institution.class);
        Program.deleteAll(Program.class);

    }

    public static int getPositionForCurrentTown(List<String> towns) {

        int positon = -1;

        for (int i = 0; i < towns.size(); i++) {

            if (towns.get(i).equals(UserController.getUserInDatabase().getTown())) {
                return i;
            }

        }

        return positon;
    }

    public static int getPositionForCurrentArea(List<String> interestAreas) {

        int positon = -1;

        for (int i = 0; i < interestAreas.size(); i++) {

            if (interestAreas.get(i).equals(UserController.getUserInDatabase().getArea())) {
                return i;
            }

        }

        return positon;
    }

    /*public static UserModel getUserInDatabase() {

        List<UserModel> userModels = UserModel.listAll(UserModel.class);

        if (userModels.size() > 0) {
            return userModels.get(0);
        }

        return null;

    }*/

    public static void saveUser(Context context, UserModel userModel) {

        SharedPreferences sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        userModel.save();
        Long id = userModel.getId();
        editor.putLong("id", id);
        editor.apply();

    }

    public static UserModel getUserInDatabase() {

        Context context = SugarApp.getSugarContext().getApplicationContext();
        SharedPreferences sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        Long id = sharedpreferences.getLong("id", -1);
        return UserModel.findById(UserModel.class, id);

    }
}
