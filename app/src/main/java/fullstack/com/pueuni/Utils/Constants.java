package fullstack.com.pueuni.Utils;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RetryPolicy;

/**
 * Created by chaycv on 26/07/17.
 */

public class Constants {

    //34.213.221.198

    public static final String LOGIN_ENDPOINT = "http://34.213.221.198:3501/users/";
    public static final String AREAS_ENDPOINT = "http://34.213.221.198:3501/resources/areas";
    public static final String TOWNS_ENDPOINT = "http://34.213.221.198:3501/resources/towns";

    public static final String USER_ENDPOINT = "http://34.213.221.198:3501/users/";
    public static final String PROGRAMS_ENDPOINT = "http://34.213.221.198:3501/programs";
    public static final String UNIVERSITIES_ENDPOINT = "http://34.213.221.198:3501/universities";
    public static final String UNIVERSITIES_ENDPOINT_QUERY = "http://34.213.221.198:3501/universities";
    public static final String CHECKIN_ENDPOINT_PHOTO = "http://34.213.221.198:3501/universities/checkin/photo/";
    public static final String TEST_ENDPOINT = "http://34.213.221.198:3501/test/";
    public static final String FIRST_TIME_OPEN = "open_first_time";

    public static String CHECKIN_ENDPOINT = "http://34.213.221.198:3501/universities/checkin/";

    public static final int timeout = 10000;

    public static RetryPolicy getPolicty() {

        return new DefaultRetryPolicy(timeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    }
}
