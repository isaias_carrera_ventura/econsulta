package fullstack.com.pueuni.Utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;

import fullstack.com.pueuni.R;

/**
 * Created by chaycv on 22/05/17.
 */

public class CustomProgressDialog {

    private ProgressDialog progressDialog;

    public CustomProgressDialog(Context context) {
        this.progressDialog = new ProgressDialog(context);
        this.progressDialog.setIndeterminate(true);
        this.progressDialog.setCancelable(false);
    }

    public void showCustomProgressDialog() {

        this.progressDialog.show();
        this.progressDialog.setContentView(R.layout.custom_progress_dialog);
        this.progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

    }

    public void dismissCustomProgressDialog() {
        // dismiss the dialog after the file was downloaded
        this.progressDialog.dismiss();
    }

}
