package fullstack.com.pueuni.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.clans.fab.FloatingActionMenu;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import fullstack.com.pueuni.Adapters.InstitutionAdapter;
import fullstack.com.pueuni.Controllers.InstitutionController;
import fullstack.com.pueuni.Models.Institution;
import fullstack.com.pueuni.R;
import fullstack.com.pueuni.Utils.Constants;
import fullstack.com.pueuni.Utils.MySingleton;

public class InstitutionFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.recycler_view_institutions_public)
    RecyclerView recyclerView;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.menu)
    FloatingActionMenu menu;

    private static final String TAG_REQUEST = "InstitutionFragment";
    private List<Institution> institutionList = new ArrayList<>();
    private InstitutionAdapter mAdapter;
    boolean institutionType;
    String key;


    private int page = 0;
    private boolean isLoading = false;
    private boolean isLastPage = false;

    public InstitutionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment3
        View rootView = inflater.inflate(R.layout.fragment_public_institution, container, false);
        ButterKnife.bind(this, rootView);

        Bundle bundle = this.getArguments();
        institutionType = bundle.getBoolean("institutions");
        swipeRefreshLayout.setOnRefreshListener(this);
        mAdapter = new InstitutionAdapter(institutionList, getActivity());
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();

                int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();

                if (!isLoading && !isLastPage) {

                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0) {

                        page = page + 1;
                        requestForUniversities();

                    }

                }
            }
        });

        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed()) {
            onStart();
        }
    }

    @Override
    public void onStart() {


        super.onStart();

        if (!getUserVisibleHint()) {
            return;
        }

        page = 0;
        institutionList.clear();
        requestForUniversities();

    }

    private void requestForUniversities() {

        if (institutionType) { //publicas
            key = "PUBLICA";
        } else {
            key = "PRIVADA";
        }

        if (!swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(true);
        }

        isLoading = true;

        final String finalKey = key;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.UNIVERSITIES_ENDPOINT + "?type=" + key + "&page=" + page, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                swipeRefreshLayout.setRefreshing(false);
                isLoading = false;

                List<Institution> institutionListAux = InstitutionController.getInstitutionListByType(finalKey, response);

                if (institutionListAux != null) {

                    if (institutionListAux.size() > 0) {

                        institutionList.addAll(institutionListAux);
                        mAdapter.notifyDataSetChanged();

                    } else {

                        isLastPage = true;

                        if (page == 0) {

                            new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText(getString(R.string.ops))
                                    .setContentText(getString(R.string.no_results_near))
                                    .show();
                        }

                    }

                } else {
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.something_went_wrong))
                            .show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

                isLoading = false;
                isLastPage = false;

                swipeRefreshLayout.setRefreshing(false);
                new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.something_went_wrong))
                        .show();
            }
        });

        stringRequest.setRetryPolicy(Constants.getPolicty());
        stringRequest.setTag(TAG_REQUEST);
        MySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);

    }

    @Override
    public void onPause() {
        super.onPause();
        MySingleton.getInstance(getActivity()).getRequestQueue().cancelAll(TAG_REQUEST);
    }

    @Override
    public void onRefresh() {
        requestForUniversities();
    }

    @OnClick(R.id.allPrograms)
    public void allProgram() {

        menu.close(true);


        isLastPage = false;
        page = 0;
        institutionList.clear();

        requestForUniversities();

    }

    @OnClick(R.id.favoritesPrograms)
    public void favoritesPrograms() {

        List<Institution> institutionListAux = InstitutionController.getFavoriteInstitutions(key);
        menu.close(true);

        if (institutionListAux != null) {

            if (institutionListAux.size() > 0) {

                institutionList.clear();
                institutionList.addAll(institutionListAux);
                mAdapter.notifyDataSetChanged();

            } else {

                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.no_favorite_college))
                        .show();

            }

        } else {

            new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                    .setTitleText(getString(R.string.ops))
                    .setContentText(getString(R.string.something_went_wrong))
                    .show();

        }

    }


}
