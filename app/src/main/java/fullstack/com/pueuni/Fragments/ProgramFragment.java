package fullstack.com.pueuni.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import fullstack.com.pueuni.Adapters.ProgramAdapter;
import fullstack.com.pueuni.Controllers.InstitutionController;
import fullstack.com.pueuni.Controllers.ProgramController;
import fullstack.com.pueuni.Models.Institution;
import fullstack.com.pueuni.Models.Program;
import fullstack.com.pueuni.R;
import fullstack.com.pueuni.Utils.Constants;
import fullstack.com.pueuni.Utils.MySingleton;

public class ProgramFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private List<Program> programList = new ArrayList<>();
    @BindView(R.id.recycler_view_program_public)
    RecyclerView recyclerView;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    private static final String TAG_REQUEST = "ProgramFragment";
    private ProgramAdapter mAdapter;

    String query;

    public ProgramFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment3
        View rootView = inflater.inflate(R.layout.fragment_program_institution, container, false);
        ButterKnife.bind(this, rootView);

        Bundle bundle = this.getArguments();
        query = bundle.getString("query");
        swipeRefreshLayout.setOnRefreshListener(this);
        mAdapter = new ProgramAdapter(programList, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);


        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed()) {
            onStart();
        }
    }

    @Override
    public void onStart() {

        super.onStart();

        if (!getUserVisibleHint()) {
            return;
        }

        requestForProgram();

    }

    private void requestForProgram() {

        if (!swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(true);
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.PROGRAMS_ENDPOINT + query, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                swipeRefreshLayout.setRefreshing(false);

                List<Program> programListAux = ProgramController.getProgramListByType(response);
                if (programListAux != null) {
                    programList.clear();
                    programList.addAll(programListAux);
                    mAdapter.notifyDataSetChanged();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefreshLayout.setRefreshing(false);
                new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.something_went_wrong))
                        .show();
            }
        });

        stringRequest.setTag(TAG_REQUEST);
        MySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
    }

    @Override
    public void onPause() {
        super.onPause();
        MySingleton.getInstance(getActivity()).getRequestQueue().cancelAll(TAG_REQUEST);
    }

    @Override
    public void onRefresh() {
        requestForProgram();
    }
}
